import nodeFetch from 'node-fetch';
import {
  PanelData,
  Variable,
  ApiResponse,
  CloudCoverageData,
  SolarActivityData
} from './types';

export const generatePanels = (): PanelData[] => {
  const values = new Array(30);
  for (let i = 0; i < 30; i++) {
    values[i] = {
      id: `P${`0${i + 1}`.slice(-2)}`,
      voltage: (Math.random() * 40 + 200).toFixed(1),
      power: (Math.random() * 100 + 300).toFixed(2)
    };
  }

  return values;
};

const API_KEY = process.env.API_KEY || '1f55f58913e549d9af7e4452cf0ed5aa';

export const fetchFromApi = (variable: Variable): Promise<ApiResponse<any>> =>
  nodeFetch(
    `http://api.planetos.com/v1/datasets/bom_access-g_global_40km/point?lon=-50.5&lat=49.5&apikey=${API_KEY}&var=${variable}&count=50`
  ).then(response => response.json());

export const fetchCloudCoverage = (): Promise<ApiResponse<CloudCoverageData>> =>
  fetchFromApi('av_ttl_cld');

export const fetchSolarActivity = (): Promise<ApiResponse<SolarActivityData>> =>
  fetchFromApi('av_swsfcdown');

export const calculateSummaryPower = (panels: PanelData[]) =>
  (panels.reduce((a, { power }) => a + parseFloat(power), 0) / 1000).toFixed(3);

export const fetchForecast = () =>
  Promise.all([fetchCloudCoverage(), fetchSolarActivity()]);

export const getHours = (time: string) => new Date(time).getHours();
