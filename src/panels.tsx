import Panel from './panel';
import Box from './generic/box';
import { PanelData } from './types';

interface Props {
  panels: PanelData[];
}

const Panels = ({ panels }: Props) => (
  <Box
    as="ul"
    style={{ display: 'flex', flexWrap: 'wrap', paddingInlineStart: 0 }}
  >
    {panels.map(({ voltage, power, id }, index) => (
      <Panel {...{ voltage, power, id, key: `panel-${index}` }} />
    ))}
  </Box>
);

export default Panels;
