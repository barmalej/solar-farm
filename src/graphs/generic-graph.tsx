import {
  VictoryChart,
  VictoryAxis,
  VictoryLine,
  VictoryStyleInterface
} from 'victory';
import { getHours } from '../utils';
import Box from '../generic/box';
import { ApiResponse, Variable, Entry } from '../types';

interface Props<T> {
  data: ApiResponse<Entry<T>>;
  variable: Variable;
  tickFormat?: (v: number) => string;
  tickDependantValues?: number[];
  style: VictoryStyleInterface;
}

function GenericGraph<T>({
  data,
  variable,
  tickFormat,
  tickDependantValues,
  style
}: Props<T>) {
  return data && data.entries && data.entries.length ? (
    <VictoryChart>
      <VictoryAxis
        label="hours"
        tickFormat={v => `${v}:00`}
        tickValues={data.entries.map(c => getHours(c.axes.time))}
      />
      <VictoryAxis
        dependentAxis
        tickFormat={tickFormat}
        tickValues={tickDependantValues}
      />
      <VictoryLine
        style={style}
        data={data.entries.map(c => ({
          y: c.data[variable],
          x: getHours(c.axes.time)
        }))}
      />
    </VictoryChart>
  ) : (
    <Box style={{ padding: '50px' }}>Failed to fetch data</Box>
  );
}

export default GenericGraph;
