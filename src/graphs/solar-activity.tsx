import GenericGraph from './generic-graph';
import { ApiResponse, SolarActivityData, SolarActivityEntry } from '../types';

interface Props {
  solarActivity: ApiResponse<SolarActivityData>;
}

const SolarActivity = ({ solarActivity }: Props) => (
  <GenericGraph<SolarActivityEntry>
    data={solarActivity}
    variable="av_swsfcdown"
    style={{ data: { stroke: 'orange' } }}
  />
);

export default SolarActivity;
