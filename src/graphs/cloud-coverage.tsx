import GenericGraph from './generic-graph';
import { ApiResponse, CloudCoverageData, CloudCoverageEntry } from '../types';

interface Props {
  cloudCoverage: ApiResponse<CloudCoverageData>;
}

const CloudCoverage = ({ cloudCoverage }: Props) => (
  <GenericGraph<CloudCoverageEntry>
    data={cloudCoverage}
    variable="av_ttl_cld"
    tickFormat={v => `${v * 100}%`}
    tickDependantValues={[0, 0.2, 0.4, 0.6, 0.8, 1]}
    style={{ data: { stroke: 'lightblue' } }}
  />
);

export default CloudCoverage;
