import Value from './value';
import panelStyle from './panel.style';
import Box from '../generic/box';

const Panel = ({ voltage, power, id }) => (
  <Box as="li" style={panelStyle}>
    <Box as="h3" style={{ marginBlockStart: 0 }}>
      {id}
    </Box>
    <Value value={voltage} unit="V" />
    <Value value={power} unit="W" />
  </Box>
);

export default Panel;
