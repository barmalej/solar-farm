import Box from '../generic/box';

interface Props {
  value: string;
  unit: 'V' | 'W';
}

const Value = ({ value, unit }: Props) => (
  <Box style={{ minWidth: '70px', display: 'flex' }}>
    <Box style={{ padding: '5px 0', width: '80%' }}>{value}</Box>
    <Box style={{ padding: '5px 0', width: '20%' }}>{unit}</Box>
  </Box>
);

export default Value;
