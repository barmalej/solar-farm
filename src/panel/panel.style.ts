import { CSSProperties } from 'react';

const panelStyle: CSSProperties | Record<string, CSSProperties> = {
  listStyleType: 'none',
  padding: '15px',
  margin: '5px',
  borderStyle: 'solid',
  borderWidth: '5px',
  borderColor: '#aaa',
  borderRadius: '5px',
  boxShadow: '5px 5px 15px 0 rgb(0, 0, 0, 0.2)',
  '@media (max-width: 768px)': { flexGrow: 1 }
};

export default panelStyle;
