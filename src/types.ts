export interface PanelData {
  id: string;
  voltage: string;
  power: string;
}

export interface Entry<T> {
  context: string;
  classifiers: {
    reference_time: string;
  };
  axes: {
    reftime: string;
    time: string;
    latitude: number;
    longitude: number;
  };
  data: T;
}

export interface ApiResponse<T> {
  stats: {
    timeMin: string;
    count: number;
    offset: number;
    nextOffset: number;
    timeMax: string;
  };
  entries: T[];
}

export type Variable = 'av_ttl_cld' | 'av_swsfcdown';

export interface CloudCoverageEntry {
  av_ttl_cld: number;
}

export interface SolarActivityEntry {
  av_swsfcdown: number;
}

export interface CloudCoverageData extends Entry<CloudCoverageEntry> {}

export interface SolarActivityData extends Entry<SolarActivityEntry> {}
