const indexStyle = {
  pageWrapper: { fontFamily: 'sans-serif' },
  graphsWrapper: {
    display: 'flex',
    '@media (max-width: 768px)': { flexDirection: 'column' }
  },
  graph: {
    flexGrow: 0.5,
    '@media (max-width: 768px)': { flexGrow: 1 }
  }
};

export default indexStyle;
