import Panels from '../src/panels';
import {
  generatePanels,
  calculateSummaryPower,
  fetchForecast
} from '../src/utils';
import SolarActivity from '../src/graphs/solar-activity';
import CloudCoverage from '../src/graphs/cloud-coverage';
import { useEffect, useState } from 'react';
import { RendererProvider } from 'react-fela';
import { createRenderer } from 'fela';
import Box from '../src/generic/box';
import indexStyle from '../src/index.style';
import {
  PanelData,
  ApiResponse,
  CloudCoverageData,
  SolarActivityData
} from '../src/types';

const renderer = createRenderer();

const SECOND = 1000;
const MINUTE = 60 * SECOND;

interface Props {
  panelsIlitial: PanelData[];
  cloudCoverageInitial: ApiResponse<CloudCoverageData>;
  solarActivityInitial: ApiResponse<SolarActivityData>;
}

const Page = ({
  panelsIlitial,
  cloudCoverageInitial,
  solarActivityInitial
}: Props) => {
  const [panels, setPanelsState] = useState(panelsIlitial);
  const [cloudCoverage, setCouldCoverageState] = useState(cloudCoverageInitial);
  const [solarActivity, setSolarActivityState] = useState(solarActivityInitial);

  useEffect(() => {
    setInterval(() => {
      const newPanels = generatePanels();

      setPanelsState(newPanels);
    }, 10 * SECOND);
    setInterval(async () => {
      const [cc, sa] = await fetchForecast();

      setCouldCoverageState(cc);
      setSolarActivityState(sa);
    }, 5 * MINUTE);
  }, []);

  return (
    <RendererProvider renderer={renderer}>
      <Box style={indexStyle.pageWrapper}>
        <Box style={indexStyle.graphsWrapper}>
          <Box style={indexStyle.graph}>
            <h3>Cloud coverage</h3>
            <CloudCoverage {...{ cloudCoverage }} />
          </Box>
          <Box style={indexStyle.graph}>
            <h3>Solar activity</h3>
            <SolarActivity {...{ solarActivity }} />
          </Box>
        </Box>
        <div>
          <strong>Total power:</strong> {calculateSummaryPower(panels)} kW/h
        </div>
        <Panels {...{ panels }} />
      </Box>
    </RendererProvider>
  );
};

Page.getInitialProps = async () => {
  const panelsIlitial = generatePanels();
  const [cloudCoverageInitial, solarActivityInitial] = await fetchForecast();

  return { panelsIlitial, cloudCoverageInitial, solarActivityInitial };
};

export default Page;
