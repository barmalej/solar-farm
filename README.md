# Solar farm test project

This app is powered by [next.js](https://nextjs.org)

With [yarn](https://yarnpkg.com/lang/en/) as package manager

## Usage:

- to run app in production mode `$ yarn build && yarn start`
- to run in development mode `$ yarn dev`

_(set `APY_KEY` environment variable to use your own key)_
